//
//  DEWRequestGenerator.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DEWRequestGenerator : NSObject

+ (instancetype)sharedInstance;

- (NSURLRequest *)generateGETRequestWithServiceClassName:(NSString *)serviceClassName
                                            requestParams:(NSDictionary *)requestParams
                                               methodName:(NSString *)methodName;

- (NSURLRequest *)generatePOSTRequestWithServiceClassName:(NSString *)serviceClassName
                                            requestParams:(NSDictionary *)requestParams
                                               methodName:(NSString *)methodName;

- (NSURLRequest *)generatePutRequestWithServiceClassName:(NSString *)serviceClassName
                                            requestParams:(NSDictionary *)requestParams
                                               methodName:(NSString *)methodName;

- (NSURLRequest *)generateDeleteRequestWithServiceClassName:(NSString *)serviceClassName
                                               requestParams:(NSDictionary *)requestParams
                                                  methodName:(NSString *)methodName;

- (NSURLRequest *)generateRequestWithServiceClassName:(NSString *)serviceClassName
                                         requestParams:(NSDictionary *)requestParams
                                            methodName:(NSString *)methodName
                                     requestWithMethod:(NSString *)method;

@end
