//
//  DEWRequestGenerator.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWRequestGenerator.h"
#import <AFNetworking.h>
#import "DEWNetworkingConfigurationManager.h"
#import "DEWService.h"
#import "NSURLRequest+DEWNetworking.h"

@interface DEWRequestGenerator ()

@property (nonatomic, strong) AFHTTPRequestSerializer *httpRequestSerializer;

@end


@implementation DEWRequestGenerator

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static DEWRequestGenerator *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DEWRequestGenerator alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    [self initialRequestGenerator];
    return self;
}

- (void)initialRequestGenerator
{
    _httpRequestSerializer = [AFJSONRequestSerializer serializer];
    _httpRequestSerializer.timeoutInterval = [DEWNetworkingConfigurationManager sharedInstance].requestTimeoutInterval;
    _httpRequestSerializer.cachePolicy = NSURLRequestUseProtocolCachePolicy;
}

- (NSURLRequest *)generateGETRequestWithServiceClassName:(NSString *)serviceClassName requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName
{
    return [self generateRequestWithServiceClassName:serviceClassName requestParams:requestParams methodName:methodName requestWithMethod:@"GET"];
}

- (NSURLRequest *)generatePOSTRequestWithServiceClassName:(NSString *)serviceClassName requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName
{
    return [self generateRequestWithServiceClassName:serviceClassName requestParams:requestParams methodName:methodName requestWithMethod:@"POST"];
}

- (NSURLRequest *)generatePutRequestWithServiceClassName:(NSString *)serviceClassName requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName
{
    return [self generateRequestWithServiceClassName:serviceClassName requestParams:requestParams methodName:methodName requestWithMethod:@"PUT"];
}

- (NSURLRequest *)generateDeleteRequestWithServiceClassName:(NSString *)serviceClassName requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName
{
    return [self generateRequestWithServiceClassName:serviceClassName requestParams:requestParams methodName:methodName requestWithMethod:@"DELETE"];
}

- (NSURLRequest *)generateRequestWithServiceClassName:(NSString *)serviceClassName requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName requestWithMethod:(NSString *)method
{
    
    DEWService *service = [[NSClassFromString(serviceClassName) alloc]init];
    NSAssert(service, @"service不存在");
    
    NSString *urlString = [service generateURLWithMethodName:methodName];
    
    NSDictionary *totalRequestParams = [self totalRequestParamsByService:service requestParams:requestParams];
    
    NSMutableURLRequest *request = [self.httpRequestSerializer requestWithMethod:method URLString:urlString parameters:totalRequestParams error:NULL];
    
    if (![method isEqualToString:@"GET"] && [DEWNetworkingConfigurationManager sharedInstance].shouldSetParamsInHTTPBodyExceptGET) {
        if (requestParams) {
             request.HTTPBody = [NSJSONSerialization dataWithJSONObject:totalRequestParams options:0 error:NULL];
        }
    }
    
    if ([service.child respondsToSelector:@selector(extraHTTPHeadParmasWithMethodName:)]) {
        NSDictionary *dict = [service.child extraHTTPHeadParmasWithMethodName:methodName];
        if (dict) {
            [dict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                [request setValue:obj forHTTPHeaderField:key];
            }];
        }
    }
    
    request.requestParams = totalRequestParams;
    return request;
}

#pragma mark - private method

//根据Service拼接额外参数

- (NSDictionary *)totalRequestParamsByService:(DEWService *)service requestParams:(NSDictionary *)requestParams
{
    NSMutableDictionary *totalRequestParams = [NSMutableDictionary dictionaryWithDictionary:requestParams];
    
    if ([service.child respondsToSelector:@selector(extraParmas)]) {
        if ([service.child extraParmas]) {
            [[service.child extraParmas] enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                [totalRequestParams setObject:obj forKey:key];
            }];
        }
    }
    return [totalRequestParams copy];
}

@end
