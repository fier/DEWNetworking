//
//  DEWCommonParamsGenerator.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/6.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DEWCommonParamsGenerator : NSObject

+ (NSDictionary *)commonParamsDictionary;
+ (NSDictionary *)commonParamsDictionaryForLog;

@end
