//
//  DEWNetworkingConfigurationManager.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DEWNetworkingConfigurationManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, assign, readonly) BOOL isReachable;
@property (nonatomic, assign) BOOL shouldCache;
@property (nonatomic, assign) BOOL serviceIsOnline;

//time settings
@property (nonatomic, assign) NSTimeInterval requestTimeoutInterval;
@property (nonatomic, assign) NSTimeInterval cacheDuration;

//最大缓存数
@property (nonatomic, assign) NSInteger cacheCountLimit;
@property (nonatomic, assign) BOOL shouldSetParamsInHTTPBodyExceptGET;

@end
