//
//  DEWNetworkingConfigurationManager.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWNetworkingConfigurationManager.h"
#import <AFNetworking.h>

@implementation DEWNetworkingConfigurationManager

+ (instancetype)sharedInstance
{
    static DEWNetworkingConfigurationManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DEWNetworkingConfigurationManager alloc] init];
        sharedInstance.shouldCache = YES;
        sharedInstance.serviceIsOnline = NO;
        sharedInstance.requestTimeoutInterval = 20.0f;
        sharedInstance.cacheDuration = 300;
        sharedInstance.cacheCountLimit = 1000;
        sharedInstance.shouldSetParamsInHTTPBodyExceptGET = YES;
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    return sharedInstance;
}

- (BOOL)isReachable
{
    
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) {
        return  NO;
    }else {
        return YES;
    }
}

- (void)setServiceIsOnline:(BOOL)serviceIsOnline
{
    _serviceIsOnline = serviceIsOnline;
#ifdef DEBUG
    if (_serviceIsOnline) {
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"com.dawa.doctor.service_is_online"];
    }else {
        [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"com.dawa.doctor.service_is_online"];
    }
#endif
}

@end
