//
//  NSArray+DEWNetworking.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "NSArray+DEWNetworking.h"

@implementation NSArray (DEWNetworking)

- (NSString *)dew_jsonString
{
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:NULL];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (NSString *)dew_paramsString
{
    
    NSMutableString *paramString = [[NSMutableString alloc] init];
    
    NSArray *sortedParams = [self sortedArrayUsingSelector:@selector(compare:)];
    [sortedParams enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([paramString length] == 0) {
            [paramString appendFormat:@"%@", obj];
        } else {
            [paramString appendFormat:@"&%@", obj];
        }
    }];
    return paramString;
}

@end
