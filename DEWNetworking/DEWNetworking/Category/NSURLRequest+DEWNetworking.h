//
//  NSURLRequest+DEWNetworking.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (DEWNetworking)

@property (nonatomic, strong) NSDictionary *requestParams;

@end
