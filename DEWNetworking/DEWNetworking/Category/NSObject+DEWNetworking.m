//
//  NSObject+DEWNetworking.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "NSObject+DEWNetworking.h"

@implementation NSObject (DEWNetworking)

- (id)dew_defaultValue:(id)defaultValue
{
    if (![defaultValue isKindOfClass:[self class]]) {
        return defaultValue;
    }
    
    if ([self dew_isEmptyObject]) {
        return defaultValue;
    }
    
    return self;
}

- (BOOL)dew_isEmptyObject
{
    if ([self isEqual:[NSNull null]]) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSString class]]) {
        if ([(NSString *)self length] == 0) {
            return YES;
        }
    }
    
    if ([self isKindOfClass:[NSArray class]]) {
        if ([(NSArray *)self count] == 0) {
            return YES;
        }
    }
    
    if ([self isKindOfClass:[NSDictionary class]]) {
        if ([(NSDictionary *)self count] == 0) {
            return YES;
        }
    }
    
    return NO;
    
}

@end
