//
//  NSDictionary+DEWNetworking.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (DEWNetworking)

- (NSString *)dew_urlParamStringSignature:(BOOL)isForSignature;
- (NSString *)dew_jsonString;
- (NSArray *)dew_transformURLParamsArraySignature:(BOOL)isForSignature;

@end
