//
//  NSMutableString+DEWNetowrking.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/6.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "NSMutableString+DEWNetowrking.h"
#import "NSObject+DEWNetworking.h"

@implementation NSMutableString (DEWNetowrking)

- (void)dew_appendURLRequest:(NSURLRequest *)request
{
    [self appendFormat:@"\n\nHTTP URL:\n\t%@", request.URL];
    [self appendFormat:@"\n\nHTTP Header:\n%@", request.allHTTPHeaderFields ? request.allHTTPHeaderFields : @"\t\t\t\t\tN/A"];
    [self appendFormat:@"\n\nHTTP Body:\n\t%@", [[[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding] dew_defaultValue:@"\t\t\t\tN/A"]];
}


@end
