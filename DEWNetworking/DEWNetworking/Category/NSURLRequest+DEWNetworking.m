//
//  NSURLRequest+DEWNetworking.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "NSURLRequest+DEWNetworking.h"
#import <objc/runtime.h>

static void *DEWNetworkingRequestParams;

@implementation NSURLRequest (DEWNetworking)

- (void)setRequestParams:(NSDictionary *)requestParams
{
    objc_setAssociatedObject(self, &DEWNetworkingRequestParams, requestParams, OBJC_ASSOCIATION_COPY);
}

- (NSDictionary *)requestParams
{
    return objc_getAssociatedObject(self, &DEWNetworkingRequestParams);
}

@end
