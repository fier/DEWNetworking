//
//  NSObject+DEWNetworking.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (DEWNetworking)

- (id)dew_defaultValue:(id)defaultValue;
- (BOOL)dew_isEmptyObject;

@end
