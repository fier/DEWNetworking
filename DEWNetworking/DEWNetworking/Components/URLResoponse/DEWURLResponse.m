//
//  DEWURLResponse.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWURLResponse.h"
#import "NSObject+DEWNetworking.h"
#import "NSURLRequest+DEWNetworking.h"

@interface DEWURLResponse ()

@property (nonatomic, assign, readwrite) DEWURLResponseStatus status;
@property (nonatomic, copy, readwrite) NSString *contentString;
@property (nonatomic, copy, readwrite) id content;
@property (nonatomic, copy, readwrite) NSURLRequest *request;
@property (nonatomic, assign, readwrite) NSInteger requestID;
@property (nonatomic, copy, readwrite) NSData *responseData;
@property (nonatomic, assign, readwrite) BOOL isCache;
@property (nonatomic, strong, readwrite) NSError *error;

@end

@implementation DEWURLResponse

- (instancetype)initWithResponseString:(NSString *)responseString requestID:(NSNumber *)requestID request:(NSURLRequest *)request responseData:(NSData *)responseData error:(NSError *)error
{

    self = [super init];
    if (self) {
        self.contentString = [responseString dew_defaultValue:@""];
        self.status = [self responseStatusWithError:error];
        self.requestID = [requestID integerValue];
        self.request = request;
        self.responseData = responseData;
        self.requestParams = request.requestParams;
        self.isCache = NO;
        self.error = error;
        if (responseData) {
            self.content = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:NULL];
        } else {
            self.content = nil;
        }
    }
    return self;
}

- (instancetype)initWithResponseString:(NSString *)responseString requestID:(NSNumber *)requestID request:(NSURLRequest *)request responseData:(NSData *)responseData status:(DEWURLResponseStatus)status
{
    
    self = [super init];
    if (self) {
        self.contentString = responseString;
        self.content = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:NULL];
        self.status = status;
        self.requestID = [requestID integerValue];
        self.request = request;
        self.responseData = responseData;
        self.requestParams = request.requestParams;
        self.isCache = NO;
        self.error = nil;
    }
    
    return self;
}

- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self) {
        self.contentString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        self.status = [self responseStatusWithError:nil];
        self.requestID = 0;
        self.request = nil;
        self.responseData = [data copy];
        self.content = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:NULL];
        self.isCache = YES;
    }
    return self;
}

#pragma mark - private methods
- (DEWURLResponseStatus)responseStatusWithError:(NSError *)error
{
    if (error) {
        DEWURLResponseStatus result = DEWURLResponseStatusErrorNoNetwork;
        
        // 除了超时以外，所有错误都当成是无网络
        if (error.code == NSURLErrorTimedOut) {
            result = DEWURLResponseStatusErrorTimeout;
        }
        return result;
    } else {
        return DEWURLResponseStatusSuccess;
    }
}

@end
