//
//  DEWURLResponse.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSUInteger,DEWURLResponseStatus){
    DEWURLResponseStatusSuccess,
    DEWURLResponseStatusErrorTimeout,
    DEWURLResponseStatusErrorNoNetwork
};


@interface DEWURLResponse : NSObject

@property (nonatomic, assign, readonly) DEWURLResponseStatus status;
@property (nonatomic, copy, readonly) NSString *contentString;//便于输出查看
@property (nonatomic, copy, readonly) id content;//JSON解析过后
@property (nonatomic, assign, readonly) NSInteger requestID;
@property (nonatomic, copy, readonly) NSURLRequest *request;
@property (nonatomic, copy, readonly) NSData *responseData;//原始返回的数据
@property (nonatomic, copy) NSDictionary *requestParams;
@property (nonatomic, strong, readonly) NSError *error;

@property (nonatomic, assign, readonly) BOOL isCache;//数据是否来自于缓存

- (instancetype)initWithResponseString:(NSString *)responseString requestID:(NSNumber *)requestID request:(NSURLRequest *)request responseData:(NSData *)responseData status:(DEWURLResponseStatus)status;

- (instancetype)initWithResponseString:(NSString *)responseString requestID:(NSNumber *)requestID request:(NSURLRequest *)request responseData:(NSData *)responseData error:(NSError *)error;

// 使用initWithData的response，它的isCache是YES，上面两个函数生成的response的isCache是NO
- (instancetype)initWithData:(NSData *)data;

@end
