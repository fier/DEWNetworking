//
//  DEWAPIProxy.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWAPIProxy.h"
#import <AFNetworking.h>

#import "DEWLogger.h"
#import "DEWRequestGenerator.h"

@interface DEWAPIProxy()

@property (nonatomic, strong) NSMutableDictionary *dispatchTable;
@property (nonatomic, strong) NSNumber *recordedRequestID;
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end


@implementation DEWAPIProxy

#pragma mark - Life cycle

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static DEWAPIProxy *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DEWAPIProxy alloc] init];
    });
    return sharedInstance;
}

- (NSInteger)callGETWithParams:(NSDictionary *)params
             serviceClassName:(NSString *)serviceClassName
                    methodName:(NSString *)methodName
                     successed:(DEWCallBack)success
                        failed:(DEWCallBack)fail
{
    NSURLRequest *request = [[DEWRequestGenerator sharedInstance]
                             generateGETRequestWithServiceClassName:serviceClassName
                             requestParams:params
                             methodName:methodName];

    NSNumber *requestId = [self callAPIWithRequest:request successed:success failed:fail];
    return [requestId integerValue];
}

- (NSInteger)callPOSTWithParams:(NSDictionary *)params
               serviceClassName:(NSString *)serviceClassName
                     methodName:(NSString *)methodName
                      successed:(DEWCallBack)success
                         failed:(DEWCallBack)fail
{

    NSURLRequest *request = [[DEWRequestGenerator sharedInstance]generatePOSTRequestWithServiceClassName:serviceClassName requestParams:params methodName:methodName];
    
    NSNumber *requestId = [self callAPIWithRequest:request successed:success failed:fail];
    return [requestId integerValue];
}

- (NSInteger)callPUTWithParams:(NSDictionary *)params
              serviceClassName:(NSString *)serviceClassName
                    methodName:(NSString *)methodName
                     successed:(DEWCallBack)success
                        failed:(DEWCallBack)fail
{

    NSURLRequest *request = [[DEWRequestGenerator sharedInstance]generatePutRequestWithServiceClassName:serviceClassName requestParams:params methodName:methodName];
    
    NSNumber *requestId = [self callAPIWithRequest:request successed:success failed:fail];
    return [requestId integerValue];

}

- (NSInteger)callDELETEWithParams:(NSDictionary *)params
                 serviceClassName:(NSString *)serviceClassName
                       methodName:(NSString *)methodName
                        successed:(DEWCallBack)success
                           failed:(DEWCallBack)fail
{
    
    NSURLRequest *request = [[DEWRequestGenerator sharedInstance]generateDeleteRequestWithServiceClassName:serviceClassName requestParams:params methodName:methodName];
    
    NSNumber *requestId = [self callAPIWithRequest:request successed:success failed:fail];
    return [requestId integerValue];
}

- (void)cancelRequestWithRequestID:(NSNumber *)requestID
{
    NSURLSessionDataTask *requestOperation = self.dispatchTable[requestID];
    [requestOperation cancel];
    [self.dispatchTable removeObjectForKey:requestID];
}

- (void)cancelRequestWithRequestIDList:(NSArray *)requestIDList
{
    for (NSNumber *requestId in requestIDList) {
        [self cancelRequestWithRequestID:requestId];
    }
}

#pragma mark - Privite

- (NSNumber *)callAPIWithRequest:(NSURLRequest *)request successed:(DEWCallBack)success failed:(DEWCallBack)fail
{
    return [self _callAPIWithRequest:request success:success failed:fail];
}

- (NSNumber *)callAPIWithUrl:(NSString *)url param:(NSDictionary *)param success:(DEWCallBack)success fail:(DEWCallBack)fail
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:param options:0 error:NULL];
    return [self _callAPIWithRequest:request.copy success:success failed:fail];
}

- (NSNumber *)_callAPIWithRequest:(NSURLRequest *)request success:(DEWCallBack)success failed:(DEWCallBack)fail
{
    
    NSLog(@"\n==================================\n\nRequest Start: \n\n %@\n\n==================================", request.URL);
    
    // 跑到这里的block的时候，就已经是主线程了。
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        NSNumber *requestID = @([dataTask taskIdentifier]);
        [self.dispatchTable removeObjectForKey:requestID];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSData *responseData = responseObject;
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if (error) {
            [DEWLogger logDebugInfoWithResponse:httpResponse
                                 responseString:responseString
                                        request:request
                                          error:error];
            DEWURLResponse *response = [[DEWURLResponse alloc] initWithResponseString:responseString requestID:requestID request:request responseData:responseData error:error];
            fail?fail(response):nil;
        } else {
            // 检查http response是否成立。
            [DEWLogger logDebugInfoWithResponse:httpResponse
                                 responseString:responseString
                                        request:request
                                          error:NULL];
            DEWURLResponse *response = [[DEWURLResponse alloc] initWithResponseString:responseString requestID:requestID request:request responseData:responseData status:DEWURLResponseStatusSuccess];
            success?success(response):nil;
        }
    }];
    
    NSNumber *requestId = @([dataTask taskIdentifier]);
    
    self.dispatchTable[requestId] = dataTask;
    [dataTask resume];
    
    return requestId;
    
}

#pragma mark - Setters and getters

- (NSMutableDictionary *)dispatchTable
{
    if (_dispatchTable) return _dispatchTable;
    _dispatchTable = [[NSMutableDictionary alloc]init];
    
    return _dispatchTable;
}

- (AFHTTPSessionManager *)sessionManager
{
    if (_sessionManager == nil) {
        _sessionManager = [AFHTTPSessionManager manager];
        _sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _sessionManager.securityPolicy.allowInvalidCertificates = YES;
        _sessionManager.securityPolicy.validatesDomainName = NO;
    }
    return _sessionManager;
}



@end
