//
//  DEWAPIProxy.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DEWURLResponse.h"

typedef void(^DEWCallBack)(DEWURLResponse *response);

@interface DEWAPIProxy : NSObject

+ (instancetype)sharedInstance;

//GET
- (NSInteger)callGETWithParams:(NSDictionary *)params serviceClassName:(NSString *)serviceClassName methodName:(NSString *)methodName successed:(DEWCallBack)success failed:(DEWCallBack)fail;
//POST
- (NSInteger)callPOSTWithParams:(NSDictionary *)params serviceClassName:(NSString *)serviceClassName methodName:(NSString *)methodName successed:(DEWCallBack)success failed:(DEWCallBack)fail;
//PUT
- (NSInteger)callPUTWithParams:(NSDictionary *)params serviceClassName:(NSString *)serviceClassName methodName:(NSString *)methodName successed:(DEWCallBack)success failed:(DEWCallBack)fail;
//DELETE
- (NSInteger)callDELETEWithParams:(NSDictionary *)params serviceClassName:(NSString *)serviceClassName methodName:(NSString *)methodName successed:(DEWCallBack)success failed:(DEWCallBack)fail;

- (NSNumber *)callAPIWithRequest:(NSURLRequest *)request successed:(DEWCallBack)success failed:(DEWCallBack)fail;

//快速发请求
- (NSNumber *)callAPIWithUrl:(NSString *)url param:(NSDictionary *)param success:(DEWCallBack)success fail:(DEWCallBack)fail;

- (void)cancelRequestWithRequestID:(NSNumber *)requestID;
- (void)cancelRequestWithRequestIDList:(NSArray *)requestIDList;

@end
