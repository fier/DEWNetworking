//
//  DEWBaseAPIManager.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/4.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWBaseAPIManager.h"
#import "DEWAPIProxy.h"
#import "DEWLogger.h"
#import "DEWNetworkingConfigurationManager.h"
#import <PINCache.h>
#import "NSDictionary+DEWNetworking.h"


@interface DEWBaseAPIManager ()

@property (nonatomic, strong, readwrite) id fetchedRawData;
@property (nonatomic, assign, readwrite) BOOL isLoading;
@property (nonatomic, assign) BOOL isNativeDataEmpty;

@property (nonatomic, copy, readwrite) NSString *errorMessage;
@property (nonatomic, readwrite) DEWAPIManagerErrorType errorType;
@property (nonatomic, strong) NSMutableArray *requestIDList;
@property (nonatomic, strong) PINCache *cache;



@end


#define AXCallAPI(REQUEST_METHOD, REQUEST_ID)                                                   \
{                                                                                               \
__weak typeof(self) weakSelf = self;                                                        \
REQUEST_ID = [[DEWAPIProxy sharedInstance] call##REQUEST_METHOD##WithParams:apiParams serviceClassName:self.child.serviceClassName methodName:self.child.methodName successed:^(DEWURLResponse *response) {                                         \
__strong typeof(weakSelf) strongSelf = weakSelf;                                        \
[strongSelf _successedOnCallingAPI:response];                                            \
} failed:^(DEWURLResponse *response) {                                                         \
__strong typeof(weakSelf) strongSelf = weakSelf;                                        \
[strongSelf _failedOnCallingAPI:response withErrorType:DEWAPIManagerErrorTypeDefault];    \
}];                                                                                         \
[self.requestIDList addObject:@(REQUEST_ID)];                                                   \
}

@implementation DEWBaseAPIManager


- (instancetype)init
{
    self = [super init];
    if (self) {
        _delegate = nil;
        _validator = nil;
        _paramSource = nil;
        
        _fetchedRawData = nil;
        
        _errorMessage = nil;
        _errorType = DEWAPIManagerErrorTypeDefault;
        
        if ([self conformsToProtocol:@protocol(DEWAPIManager)]) {
            self.child = (id <DEWAPIManager>)self;
        } else {
            self.child = (id <DEWAPIManager>)self;
            NSException *exception = [[NSException alloc] initWithName:@"DEWBaseAPIManager提示" reason:[NSString stringWithFormat:@"%@没有遵循DEWAPIManager协议",self.child] userInfo:nil];
            @throw exception;
        }
    }

    return self;
}

#pragma mark - Public method

- (void)cancelAllRequests
{
    [[DEWAPIProxy sharedInstance] cancelRequestWithRequestIDList:self.requestIDList];
    [self.requestIDList removeAllObjects];
}

- (void)cancelRequestWithRequestID:(NSInteger)requestID
{
    
    [self _removeRequestID:requestID];
    [[DEWAPIProxy sharedInstance]cancelRequestWithRequestID:@(requestID)];
}

- (id)fetchDataWithReformer:(id<DEWAPIManagerDataReformer>)reformer
{
    
    id resultData = nil;
    if ([reformer respondsToSelector:@selector(manager:reformData:)]) {
        resultData = [reformer manager:self reformData:self.fetchedRawData];
    } else {
        resultData = [self.fetchedRawData mutableCopy];
    }
    return resultData;
}

#pragma mark - Calling API

- (NSInteger)loadData
{
    return [self _loadDataWithOption:DEWAPIManagerLoadDataOptionDefault];
}

- (NSInteger)loadDataWithOption:(DEWAPIManagerLoadDataOption)option
{
    return [self _loadDataWithOption:option];
}

- (NSInteger)_loadDataWithOption:(DEWAPIManagerLoadDataOption)option
{
    if (self.isLoading && [self.child prohibitMutiRequest]) {
        return 0;
    }
    
    self.loadOption = option;
    NSDictionary *params = [self.paramSource paramsForAPI:self];
    NSInteger requestId = [self loadDataWithParams:params];
    
    return requestId;
}

- (NSInteger)loadDataWithParams:(NSDictionary *)params
{
    NSInteger requestId = 0;
    NSDictionary *apiParams = [self reformParams:params];
    
    if ([self shouldCallAPIWithParams:apiParams]) {
        if ([self.validator manager:self isCorrectWithParamsData:apiParams]) {

            //首先得开启缓存
            if ([self.child shouldCache]) {
                
                switch (self.loadOption) {
                    case DEWAPIManagerLoadDataOptionIngoreCache: {
                        
                    }break;
                    case DEWAPIManagerLoadDataOptionFromCache: {
                        if ([self _hasCacheWithParams:apiParams]) {
                            return 0;
                        }
                    }break;
                    case DEWAPIManagerLoadDataOptionFromCacheAndRefreshCache: {
                        [self _hasCacheWithParams:apiParams];
                    }break;
                        
                    default: {
                        if ([self _hasCacheWithParams:apiParams]) {
                            return 0;
                        }
                    }break;
                }
        
            }
            
            //实际的网络请求
            if ([self isReachable]) {
                self.isLoading = YES;
                switch (self.child.requestType)
                {
                    case DEWAPIManagerRequestTypeGet:
                        AXCallAPI(GET, requestId);
                        break;
                    case DEWAPIManagerRequestTypePost:
                        AXCallAPI(POST, requestId);
                        break;
                    case DEWAPIManagerRequestTypePut:
                        AXCallAPI(PUT, requestId);
                        break;
                    case DEWAPIManagerRequestTypeDelete:
                        AXCallAPI(DELETE, requestId);
                        break;
                    default:
                        break;
                }
                
                NSMutableDictionary *params = [apiParams mutableCopy];
                params[kDEWBaseAPIManagerRequestID] = @(requestId);
                [self afterCallingAPIWithParams:params];
                return requestId;
                
            } else {
                [self _failedOnCallingAPI:nil withErrorType:DEWAPIManagerErrorTypeNoNetWork];
                return requestId;
            }
        } else {
            [self _failedOnCallingAPI:nil withErrorType:DEWAPIManagerErrorTypeParamsError];
            return requestId;
        }
    }
    return requestId;
}

#pragma mark - Method for interceptor

- (BOOL)beforePerformSuccessWithResponse:(DEWURLResponse *)response
{

    BOOL result = YES;
    self.errorType = DEWAPIManagerErrorTypeSuccess;
    if (self != self.interceptor && [self.interceptor respondsToSelector:@selector(manager: beforePerformSuccessWithResponse:)]) {
        result = [self.interceptor manager:self beforePerformSuccessWithResponse:response];
    }
    return result;
}

- (void)afterPerformSuccessWithResponse:(DEWURLResponse *)response
{
    if (self != self.interceptor && [self.interceptor respondsToSelector:@selector(manager:afterPerformSuccessWithResponse:)]) {
        [self.interceptor manager:self afterPerformSuccessWithResponse:response];
    }

}

- (BOOL)beforePerformFailWithResponse:(DEWURLResponse *)response
{
    BOOL result = YES;
    if (self != self.interceptor && [self.interceptor respondsToSelector:@selector(manager:beforePerformFailWithResponse:)]) {
        result = [self.interceptor manager:self beforePerformFailWithResponse:response];
    }
    return result;

}

- (void)afterPerformFailWithResponse:(DEWURLResponse *)response
{
    
    if (self != self.interceptor && [self.interceptor respondsToSelector:@selector(manager:afterPerformFailWithResponse:)]) {
        [self.interceptor manager:self afterPerformFailWithResponse:response];
    }
}

- (BOOL)shouldCallAPIWithParams:(NSDictionary *)params
{
    if (self != self.interceptor && [self.interceptor respondsToSelector:@selector(manager:shouldCallAPIWithParams:)]) {
        return [self.interceptor manager:self shouldCallAPIWithParams:params];
    } else {
        return YES;
    }
}

- (void)afterCallingAPIWithParams:(NSDictionary *)params
{
    if (self != self.interceptor && [self.interceptor respondsToSelector:@selector(manager:afterCallingAPIWithParams:)]) {
        [self.interceptor manager:self afterCallingAPIWithParams:params];
    }
}

#pragma mark - Method for child

- (void)cleanData
{
    [self.cache removeAllObjects];
    self.fetchedRawData = nil;
    self.errorMessage = nil;
    self.errorType = DEWAPIManagerErrorTypeDefault;
}

- (NSDictionary *)reformParams:(NSDictionary *)params
{
    IMP childIMP = [self.child methodForSelector:@selector(reformParams:)];
    IMP selfIMP = [self methodForSelector:@selector(reformParams:)];
    
    if (childIMP == selfIMP) {
        return params;
    } else {
        // 如果child是继承得来的，那么这里就不会跑到，会直接跑子类中的IMP。
        // 如果child是另一个对象，就会跑到这里
        NSDictionary *result = nil;
        result = [self.child reformParams:params];
        if (result) {
            return result;
        } else {
            return params;
        }
    }
}

- (BOOL)shouldCache
{
    return [DEWNetworkingConfigurationManager sharedInstance].shouldCache;
}

#pragma mark - Priviete

- (void)_removeRequestID:(NSInteger)requestID
{
    
    NSNumber *requestIDToRemove = nil;
    for (NSNumber *storedRequestId in self.requestIDList) {
        if ([storedRequestId integerValue] == requestID) {
            requestIDToRemove = storedRequestId;
        }
    }
    if (requestIDToRemove) {
        [self.requestIDList removeObject:requestIDToRemove];
    }
}

- (void)_loadDataFromNative
{
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[[NSUserDefaults standardUserDefaults] dataForKey:self.child.methodName] options:0 error:NULL];
    
    if (result) {
        self.isNativeDataEmpty = NO;
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            DEWURLResponse *response = [[DEWURLResponse alloc] initWithData:[NSJSONSerialization dataWithJSONObject:result options:0 error:NULL]];
            [strongSelf _successedOnCallingAPI:response];
        });
    } else {
        self.isNativeDataEmpty = YES;
    }
}

- (BOOL)_hasCacheWithParams:(NSDictionary *)params
{
    
    NSString *serviceClassName = self.child.serviceClassName;
    NSString *methodName = self.child.methodName;
    
    NSString *key = [self _keyWithServiceClassName:serviceClassName methodName:methodName requestParams:params];
    NSData *result = [self.cache objectForKey:key];
    
    if (result == nil) {
        return NO;
    }
    
    DEWService *service = [[NSClassFromString(serviceClassName) alloc]init];
    if (!service) return NO;
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        __strong typeof (weakSelf) strongSelf = weakSelf;
        DEWURLResponse *response = [[DEWURLResponse alloc] initWithData:result];
        response.requestParams = params;
        
        [DEWLogger logDebugInfoWithCachedResponse:response methodName:methodName service:service];
        [strongSelf _successedOnCallingAPI:response];
        
    });
    
    return YES;
}

- (void)_successedOnCallingAPI:(DEWURLResponse *)response
{
    
    self.isLoading = NO;
    self.response = response;
    
    if (response.content) {
        self.fetchedRawData = [response.content copy];
    } else {
        self.fetchedRawData = [response.responseData copy];
    }
    
    [self _removeRequestID:response.requestID];
    
    NSString *serviceClassName = self.child.serviceClassName;
    DEWService *service = [[NSClassFromString(serviceClassName) alloc]init];
    if (service) {
        if ([service.child respondsToSelector:@selector(callBackBySuccessOnCallingAPI:)]) {
            [service.child callBackBySuccessOnCallingAPI:response];
        }
    }
    
    //now ,data will be cached if request success,rather than data success
    if ([self shouldCache] && !response.isCache) {
        
        NSDictionary *params =  response.requestParams;
        NSString *serviceClassName = self.child.serviceClassName;
        NSString *methodName = self.child.methodName;
        
        NSString *cacheKey = [self _keyWithServiceClassName:serviceClassName methodName:methodName requestParams:params];
        
        [self.cache setObject:response.responseData forKey:cacheKey block:^(PINCache * _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
            
        }];
    }
    
    //if self.validator == nil  handle
    if ([self.validator manager:self isCorrectWithCallBackData:response.content]) {
        
        if ([self beforePerformSuccessWithResponse:response]) {
            [self.delegate managerCallAPIDidSuccess:self];
        }
        
        [self afterPerformSuccessWithResponse:response];
    } else {
        [self _failedOnCallingAPI:response withErrorType:DEWAPIManagerErrorTypeNoContent];
    }
}

- (void)_failedOnCallingAPI:(DEWURLResponse *)response withErrorType:(DEWAPIManagerErrorType)errorType
{
    
    NSString *serviceClassName = self.child.serviceClassName;
    DEWService *service = [[NSClassFromString(serviceClassName) alloc]init];
    
    if (!service) return;
    
    self.isLoading = NO;
    self.response = response;
    BOOL needCallBack = YES;

    if ([service.child respondsToSelector:@selector(shouldCallBackByFailedOnCallingAPI:errorType:)]) {
        needCallBack = [service.child shouldCallBackByFailedOnCallingAPI:response errorType:errorType];
    }
    
    //由service决定是否结束回调
    if (!needCallBack) {
        return;
    }
    
    //继续错误的处理
    self.errorType = errorType;
    [self _removeRequestID:response.requestID];
    
    if (response.content) {
        self.fetchedRawData = [response.content copy];
    } else {
        self.fetchedRawData = [response.responseData copy];
    }
    
    if ([self beforePerformFailWithResponse:response]) {
        [self.delegate managerCallAPIDidFailed:self];
    }
    [self afterPerformFailWithResponse:response];
}

- (NSString *)_keyWithServiceClassName:(NSString *)serviceClassName
                           methodName:(NSString *)methodName
                        requestParams:(NSDictionary *)requestParams
{
    NSString *requestParamString = [requestParams dew_urlParamStringSignature:NO];
    return [NSString stringWithFormat:@"%@%@%@%@", serviceClassName, methodName, requestParamString?requestParamString:@"", self.cacheKeyRefer ? self.cacheKeyRefer : @""];
}

#pragma mark - Dealloc

- (void)dealloc
{
    [self cancelAllRequests];
    self.requestIDList = nil;
}

- (PINCache *)cache
{
    if (_cache) return _cache;
    
    NSString *cacheName = [NSString stringWithFormat:@"%@",NSStringFromClass([self.child class])];
    
    _cache = [[PINCache alloc]initWithName:cacheName];
    _cache.diskCache.ageLimit  = self.cacheAgeLimit;
    _cache.memoryCache.ageLimit = self.cacheAgeLimit;
    
    return _cache;
}

- (NSMutableArray *)requestIDList
{
    if (_requestIDList) return _requestIDList;
    _requestIDList = [[NSMutableArray alloc]init];
    return _requestIDList;
}

- (BOOL)isReachable
{
    BOOL isReachability = [DEWNetworkingConfigurationManager sharedInstance].isReachable;
    if (!isReachability) {
        self.errorType =  DEWAPIManagerErrorTypeNoNetWork;
    }
    return isReachability;
}

- (BOOL)isLoading
{
    if (self.requestIDList.count == 0) {
        _isLoading = NO;
    }
    return _isLoading;
}

- (BOOL)prohibitMutiRequest
{
    return NO;
}

- (NSTimeInterval)cacheAgeLimit
{
    return [DEWNetworkingConfigurationManager sharedInstance].cacheDuration;
}

- (NSString *)cacheKeyRefer
{
    return nil;
}

@end
