//
//  DEWBaseAPIManager.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/4.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DEWURLResponse.h"

@class DEWBaseAPIManager;

//每次请求都会生成一个唯一的ID
static NSString * const kDEWBaseAPIManagerRequestID = @"kDEWBaseAPIManagerRequestID";

// 请求回调
@protocol DEWAPICallBackDelegate <NSObject>

@required
- (void)managerCallAPIDidSuccess:(DEWBaseAPIManager *)manager;
- (void)managerCallAPIDidFailed:(DEWBaseAPIManager *)manager;

@end

//数据格式转换器
@protocol DEWAPIManagerDataReformer <NSObject>

- (id)manager:(DEWBaseAPIManager *)manager reformData:(NSDictionary *)data;
@optional
-(id)manager:(DEWBaseAPIManager *)manager failedReform:(NSDictionary *)data;

@end

//验证器
@protocol DEWAPIManagerValidator <NSObject>

@required
- (BOOL)manager:(DEWBaseAPIManager *)manager isCorrectWithCallBackData:(NSDictionary *)data;
- (BOOL)manager:(DEWBaseAPIManager *)manager isCorrectWithParamsData:(NSDictionary *)data;

@end


//让manager获取调用API所需的数据
@protocol DEWAPIManagerParamSource <NSObject>

@required
- (NSDictionary *)paramsForAPI:(DEWBaseAPIManager *)manager;

@end

typedef NS_ENUM (NSUInteger, DEWAPIManagerErrorType){
    DEWAPIManagerErrorTypeDefault,       //没有产生过API请求，这个是manager的默认状态。
    DEWAPIManagerErrorTypeSuccess,       //API请求成功且返回数据正确，此时manager的数据是可以直接拿来使用的。
    DEWAPIManagerErrorTypeNoContent,     //API请求成功但返回数据不正确。如果回调数据验证函数返回值为NO，manager的状态就会是这个。
    DEWAPIManagerErrorTypeParamsError,   //参数错误，此时manager不会调用API，因为参数验证是在调用API之前做的。
    DEWAPIManagerErrorTypeTimeout,       //请求超时。
    DEWAPIManagerErrorTypeNoNetWork      //网络不通。在调用API之前会判断一下当前网络是否通畅，这个也是在调用API之前验证的，和上面超时的状态是有区别的。
};

typedef NS_ENUM (NSUInteger, DEWAPIManagerRequestType){
    DEWAPIManagerRequestTypeGet,
    DEWAPIManagerRequestTypePost,
    DEWAPIManagerRequestTypePut,
    DEWAPIManagerRequestTypeDelete
};


@protocol DEWAPIManager <NSObject>

@required
- (NSString *)methodName;
- (NSString *)serviceClassName;
- (DEWAPIManagerRequestType)requestType;
- (BOOL)shouldCache;//如果是YES，则只要能读到缓存，就不会发送请求

@optional
- (void)cleanData;
- (NSDictionary *)reformParams:(NSDictionary *)param;
- (NSInteger)loadDataWithParams:(NSDictionary *)params;
- (BOOL)prohibitMutiRequest;//是否禁止相同请求同时进行
- (NSString *)cacheKeyRefer;

//缓存失效间隔
- (NSTimeInterval)cacheAgeLimit;

@end

//拦截器
@protocol DEWAPIManagerInterceptor <NSObject>

@optional
- (BOOL)manager:(DEWBaseAPIManager *)manager beforePerformSuccessWithResponse:(DEWURLResponse *)response;
- (void)manager:(DEWBaseAPIManager *)manager afterPerformSuccessWithResponse:(DEWURLResponse *)response;

- (BOOL)manager:(DEWBaseAPIManager *)manager beforePerformFailWithResponse:(DEWURLResponse *)response;
- (void)manager:(DEWBaseAPIManager *)manager afterPerformFailWithResponse:(DEWURLResponse *)response;

- (BOOL)manager:(DEWBaseAPIManager *)manager shouldCallAPIWithParams:(NSDictionary *)params;
- (void)manager:(DEWBaseAPIManager *)manager afterCallingAPIWithParams:(NSDictionary *)params;

@end

typedef enum : NSUInteger {
    DEWAPIManagerLoadDataOptionDefault,
    DEWAPIManagerLoadDataOptionIngoreCache,
    DEWAPIManagerLoadDataOptionFromCache,
    DEWAPIManagerLoadDataOptionFromCacheAndRefreshCache
    //如果没有cache ，才发请求
} DEWAPIManagerLoadDataOption;//数据的读取来源

@interface DEWBaseAPIManager : NSObject

@property (nonatomic, weak) id<DEWAPICallBackDelegate> delegate;
@property (nonatomic, weak) id<DEWAPIManagerParamSource> paramSource;
@property (nonatomic, weak) id<DEWAPIManagerValidator> validator;

@property (nonatomic, weak) NSObject<DEWAPIManager> *child; //里面会调用到NSObject的方法，所以这里不用id
@property (nonatomic, weak) id<DEWAPIManagerInterceptor> interceptor;

@property (nonatomic, copy, readonly) NSString *errorMessage;
@property (nonatomic, readonly) DEWAPIManagerErrorType errorType;
@property (nonatomic, strong) DEWURLResponse *response;

@property (nonatomic, assign, readonly) BOOL isReachable;
@property (nonatomic, assign, readonly) BOOL isLoading;
@property (nonatomic, strong) NSString *cacheKeyRefer;


@property (nonatomic, assign) DEWAPIManagerLoadDataOption loadOption;

- (id)fetchDataWithReformer:(id<DEWAPIManagerDataReformer>)reformer;

- (NSInteger)loadData;
- (NSInteger)loadDataWithOption:(DEWAPIManagerLoadDataOption)option;


- (void)cancelAllRequests;
- (void)cancelRequestWithRequestID:(NSInteger)requestID;

- (BOOL)beforePerformSuccessWithResponse:(DEWURLResponse *)response;
- (void)afterPerformSuccessWithResponse:(DEWURLResponse *)response;

- (BOOL)beforePerformFailWithResponse:(DEWURLResponse *)response;
- (void)afterPerformFailWithResponse:(DEWURLResponse *)response;

- (BOOL)shouldCallAPIWithParams:(NSDictionary *)params;
- (void)afterCallingAPIWithParams:(NSDictionary *)params;

//用于子类重载
- (NSDictionary *)reformParams:(NSDictionary *)params;


- (void)cleanData;
- (BOOL)shouldCache;
- (BOOL)prohibitMutiRequest;
- (NSTimeInterval)cacheAgeLimit;

@end







