//
//  DEWLogger.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/6.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DEWService.h"
#import "DEWLoggerConfiguration.h"
#import "DEWURLResponse.h"

@interface DEWLogger : NSObject

@property (nonatomic ,strong, readonly) DEWLoggerConfiguration *configParams;

+ (NSString *)logDebugInfoWithRequest:(NSURLRequest *)request
                        apiName:(NSString *)apiName
                        service:(DEWService *)service
                  requestParams:(id)requestParams
                     httpMethod:(NSString *)httpMethod;

+ (NSString *)logDebugInfoWithResponse:(NSHTTPURLResponse *)response
                  responseString:(NSString *)responseString
                         request:(NSURLRequest *)request
                           error:(NSError *)error;

+ (NSString *)logDebugInfoWithCachedResponse:(DEWURLResponse *)response
                            methodName:(NSString *)methodName
                     service:(DEWService *)service;

+ (instancetype)sharedInstance;


//- (void)logWithActionCode:(NSString *)actionCode params:(NSDictionary *)params;


@end
