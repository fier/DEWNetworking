//
//  DEWLogger.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/6.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWLogger.h"
#import "NSObject+DEWNetworking.h"
#import "NSMutableString+DEWNetowrking.h"
#import "NSURLRequest+DEWNetworking.h"
#import "NSArray+DEWNetworking.h"
#import "DEWCommonParamsGenerator.h"
#import "DEWAPIProxy.h"

@interface DEWLogger ()

@property (nonatomic, strong, readwrite) DEWLoggerConfiguration *configParams;

@end



@implementation DEWLogger

+ (NSString *)logDebugInfoWithRequest:(NSURLRequest *)request apiName:(NSString *)apiName service:(DEWService *)service requestParams:(id)requestParams httpMethod:(NSString *)httpMethod
{
#ifdef DEBUG
    BOOL isOnline = NO;
    if ([service respondsToSelector:@selector(isOnline)]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[service methodSignatureForSelector:@selector(isOnline)]];
        invocation.target = service;
        invocation.selector = @selector(isOnline);
        [invocation invoke];
        [invocation getReturnValue:&isOnline];
    }
    
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n**************************************************************\n*                       Request Start                        *\n**************************************************************\n\n"];
    
    [logString appendFormat:@"API Name:\t\t%@\n", [apiName dew_defaultValue:@"N/A"]];
    [logString appendFormat:@"Method:\t\t\t%@\n", [httpMethod dew_defaultValue:@"N/A"]];
    [logString appendFormat:@"Version:\t\t%@\n", [service.apiVersion dew_defaultValue:@"N/A"]];
    [logString appendFormat:@"Service:\t\t%@\n", [service class]];
    [logString appendFormat:@"Status:\t\t\t%@\n", isOnline ? @"online" : @"offline"];
    [logString appendFormat:@"Params:\n%@", requestParams];
    
    [logString dew_appendURLRequest:request];
    
    [logString appendFormat:@"\n\n**************************************************************\n*                         Request End                        *\n**************************************************************\n\n\n\n"];
    NSLog(@"%@", logString);
    return logString;
#else
    return @"";
#endif
}

+ (NSString *)logDebugInfoWithResponse:(NSHTTPURLResponse *)response responseString:(NSString *)responseString request:(NSURLRequest *)request error:(NSError *)error
{
#ifdef DEBUG
    BOOL shouldLogError = error ? YES : NO;
    
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n==============================================================\n=                        API Response                        =\n==============================================================\n\n"];
    
    [logString appendFormat:@"Status:\t%ld\t(%@)\n\n", (long)response.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:response.statusCode]];
    [logString appendFormat:@"Content:\n\t%@\n\n", responseString];
    if (shouldLogError) {
        [logString appendFormat:@"Error Domain:\t\t\t\t\t\t\t%@\n", error.domain];
        [logString appendFormat:@"Error Domain Code:\t\t\t\t\t\t%ld\n", (long)error.code];
        [logString appendFormat:@"Error Localized Description:\t\t\t%@\n", error.localizedDescription];
        [logString appendFormat:@"Error Localized Failure Reason:\t\t\t%@\n", error.localizedFailureReason];
        [logString appendFormat:@"Error Localized Recovery Suggestion:\t%@\n\n", error.localizedRecoverySuggestion];
    }
    
    [logString appendString:@"\n---------------  Related Request Content  --------------\n"];
    
    [logString dew_appendURLRequest:request];
    
    [logString appendFormat:@"\n\n==============================================================\n=                        Response End                        =\n==============================================================\n\n\n\n"];
    
    NSLog(@"%@", logString);
    return logString;
#else
    return @"";
#endif
}

+ (NSString *)logDebugInfoWithCachedResponse:(DEWURLResponse *)response methodName:(NSString *)methodName service:(DEWService *)service
{
#ifdef DEBUG
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n==============================================================\n=                      Cached Response                       =\n==============================================================\n\n"];
    
    [logString appendFormat:@"API Name:\t\t%@\n", [methodName dew_defaultValue:@"N/A"]];
    [logString appendFormat:@"Version:\t\t%@\n", [service.apiVersion dew_defaultValue:@"N/A"]];
    [logString appendFormat:@"Service:\t\t%@\n", [service class]];
    [logString appendFormat:@"Method Name:\t%@\n", methodName];
    [logString appendFormat:@"Params:\n%@\n\n", response.requestParams];
    [logString appendFormat:@"Content:\n\t%@\n\n", response.contentString];
    
    [logString appendFormat:@"\n\n==============================================================\n=                        Response End                        =\n==============================================================\n\n\n\n"];
    NSLog(@"%@", logString);
    return logString;
#else
 return @"";
#endif

}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static DEWLogger *sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.configParams = [[DEWLoggerConfiguration alloc] init];
    }
    return self;
}

//- (void)logWithActionCode:(NSString *)actionCode params:(NSDictionary *)params
//{
//    
//    NSMutableDictionary *actionDict = [[NSMutableDictionary alloc] init];
//    actionDict[@"act"] = actionCode;
//    [actionDict addEntriesFromDictionary:params];
//    [actionDict addEntriesFromDictionary:[DEWCommonParamsGenerator commonParamsDictionaryForLog]];
//    NSDictionary *logJsonDict = @{self.configParams.sendActionKey:[@[actionDict] DEW_jsonString]};
//    
//    [[DEWAPIProxy sharedInstance] callPOSTWithParams:logJsonDict serviceClassName:self.configParams.serviceClassName methodName:self.configParams.sendActionMethod successed:nil failed:nil];
//
//}


@end
