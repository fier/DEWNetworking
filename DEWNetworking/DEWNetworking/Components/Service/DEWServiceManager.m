//
//  DEWServiceManager.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/6.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWServiceManager.h"

@implementation DEWServiceManager

+ (BOOL)isServiceValid:(NSString *)serviceClassName
{
   return [[NSClassFromString(serviceClassName) alloc]init] != nil;
}

@end


