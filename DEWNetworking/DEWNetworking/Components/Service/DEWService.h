//
//  DEWService.h
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DEWURLResponse.h"
#import "DEWBaseAPIManager.h"

@protocol DEWServiceProtocol <NSObject>

@property (nonatomic, readonly) BOOL isOnline;

@property (nonatomic, readonly) NSString *offlineAPIBaseURL;
@property (nonatomic, readonly) NSString *onlineAPIBaseURL;

@property (nonatomic, readonly) NSString *offlineAPIVersion;
@property (nonatomic, readonly) NSString *onlineAPIVersion;

@optional

/**
 给每个APIManager 配置通用参数
 */
- (NSDictionary *)extraParmas;

/**
 给请求头中添加额外参数
 */
- (NSDictionary *)extraHTTPHeadParmasWithMethodName:(NSString *)method;

/**
 所有APIManager请求失败的通用处理
 */
- (BOOL)shouldCallBackByFailedOnCallingAPI:(DEWURLResponse *)response errorType:(DEWAPIManagerErrorType)errorType;

- (void)callBackBySuccessOnCallingAPI:(DEWURLResponse *)response;

@end


@interface DEWService : NSObject

@property (class ,nonatomic, strong) NSString *apiBaseUrl;
@property (nonatomic, strong, readonly) NSString *apiBaseUrl;
@property (nonatomic, strong, readonly) NSString *apiVersion;

@property (nonatomic, weak, readonly) id<DEWServiceProtocol> child;

- (NSString *)generateURLWithMethodName:(NSString *)method;

@end
