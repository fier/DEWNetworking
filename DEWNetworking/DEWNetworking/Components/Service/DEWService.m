//
//  DEWService.m
//  DEWNetworking
//
//  Created by Shaw on 2017/9/5.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWService.h"

@interface DEWService()

@property (nonatomic, weak, readwrite) id<DEWServiceProtocol> child;

@end


@implementation DEWService

- (instancetype)init
{
    self = [super init];
    if (self) {
        if ([self conformsToProtocol:@protocol(DEWServiceProtocol)]) {
            self.child = (id<DEWServiceProtocol>)self;
        }
    }
    return self;
}

- (NSString *)generateURLWithMethodName:(NSString *)method
{
    NSMutableString *urlString = [[NSMutableString alloc]init];
    [urlString appendString:self.apiBaseUrl];
    if (self.apiVersion.length != 0) {
        [urlString appendString:[NSString stringWithFormat:@"/%@",self.apiVersion]];
    }
    
    if (method.length != 0) {
        [urlString appendString:[NSString stringWithFormat:@"/%@",method]];
    }
    
    return urlString;
}

#pragma mark - getters and setters

+ (NSString *)apiBaseUrl
{
    DEWService *service = [[self alloc]init];
    return service.apiBaseUrl;
}

+ (void)setApiBaseUrl:(NSString *)apiBaseUrl
{

}

- (NSString *)apiBaseUrl
{
    return self.child.isOnline ? self.child.onlineAPIBaseURL : self.child.offlineAPIBaseURL;
}

- (NSString *)apiVersion
{
    return self.child.isOnline ? self.child.onlineAPIVersion : self.child.offlineAPIVersion;
}

@end
